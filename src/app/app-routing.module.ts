import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { GetboardsComponent } from './getboards/getboards.component';
import { SubboardsComponent } from './subboards/subboards.component';
const routes: Routes = [
  
  { path: '', component: GetboardsComponent },
  { path: 'tasks', component: SubboardsComponent }
  
]
@NgModule({
  exports: [ RouterModule ],
  imports: [ RouterModule.forRoot(routes,{ useHash: true }), CommonModule ],
  declarations: []
})
export class AppRoutingModule { }
export const routingComponents = [GetboardsComponent,SubboardsComponent]
