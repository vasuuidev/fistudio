import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './/app-routing.module';
import { GetboardsComponent } from './getboards/getboards.component';
import { FormsModule }   from '@angular/forms';
import { SubboardsComponent } from './subboards/subboards.component';
import { myService } from './services/ser';
import { HeaderComponent } from './header/header.component';

@NgModule({
  declarations: [
    AppComponent,
    GetboardsComponent,
    SubboardsComponent,
    HeaderComponent
  ],
  imports: [
    BrowserModule,AppRoutingModule, HttpClientModule,FormsModule
  ],
  providers: [myService],
  bootstrap: [AppComponent]
})
export class AppModule { }
