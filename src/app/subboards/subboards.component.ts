import { Component, OnInit ,Input} from '@angular/core';
import { getdet } from './Service';
import { myService } from '../services/ser';
import { NgForm } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-subboards',
  templateUrl: './subboards.component.html',
  styleUrls: ['./subboards.component.css'],
  providers: [getdet]
})

export class SubboardsComponent implements OnInit {
    getboards:any;
    message:any;
    getlists:any;
    gettask:any;
    totallist:any[];
    createopen:boolean;
    addtaskopen:boolean;
    addtaskclose:boolean;
    createclose:boolean;
    addlistval:any;
    
  constructor(private _getboards: getdet,private router: Router,private _myService: myService) { 
    this.totallist=[];
    this.createclose=false;
    this.createopen=true;
    this.addtaskclose=false;
    this.addtaskopen=true;
    

}
getdata()
{
  this.totallist=[];
  var d = this._myService.getData();
    if (d == "")
    {
     this.router.navigate(['/']);
    }
    else
    {
     this._getboards.getboards(d)
       .subscribe(
       getboards => {
         this.getboards=getboards;
        
       },
       suppoorteroor => {
              });
    this._getboards.getlist(d)
       .subscribe(
       getlists => {
         this.getlists=getlists;
         for (var i =0; i < this.getlists.lists.length; i++)
         {

           
           this._getboards.gettasks(this.getlists.lists[i]._id)
       .subscribe(
       gettask => {

         this.gettask=gettask;
         
if (this.gettask.task  !== null)
         {
         var obj = findObjectByKey(this.getlists.lists, '_id', this.gettask.task.list_id);

         console.log(obj);
         obj.task=this.gettask
         console.log("this.getlists.lists",this.getlists.lists);
        //  this.gettask.idv=obj._id;
        //  this.gettask.name=obj.list_name;
       }
       function findObjectByKey(array, key, value) {
    for (var i = 0; i < array.length; i++) {
        if (array[i][key] === value) {
            return array[i];
        }
    }
    return null;
}
       
       },
       suppoorteroor => {
              });
         }
         
        
       },
       suppoorteroor => {
              });

      
       console.log(this.totallist);


  }

}
openpop()
{
  this.createopen=false;
  this.createclose=true;
}
addtaskclosepop()
{
  this.addtaskclose=false;
}
addlistv(request, form: NgForm)
{
var supportbody=[{
  task_name: form.value,
  
},{id: this.addlistval}]
     this._getboards.crttask(supportbody)
       .subscribe(
       tasks => {
         this.addtaskclose=false;
         this.getdata();
              //this.router.navigate(['/']);
        
       },
       suppoorteroor => {
              });
}
addtaskopenpop(d)
{
this.addtaskclose=true;
this.addlistval=d;
    //this.addtaskopen=false;
}
closepop()
{
   this.createopen=true;
  this.createclose=false;
}
crtlist( request, form: NgForm)
{
    var supportbody=[{
  list_name: form.form.value.listname,
  
},{id: this._myService.getData()}]
     this._getboards.crtlist(supportbody)
       .subscribe(
       listboards => {
         this.createopen=true;
  this.createclose=false;
         //window.location.reload();
         this.getdata();
         
  
              //this.router.navigate(['/']);
        
       },
       suppoorteroor => {
              });
}
  ngOnInit() {
  this.getdata();
  
  }

}
