import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubboardsComponent } from './subboards.component';

describe('SubboardsComponent', () => {
  let component: SubboardsComponent;
  let fixture: ComponentFixture<SubboardsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubboardsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubboardsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
