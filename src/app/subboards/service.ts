import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';

//import 'rxjs/add/observable/throw';
import { Observable } from 'rxjs';

 

@Injectable()
export class getdet {


  constructor(private _http:  HttpClient) { 
  }

  getboards(request) {
    return this._http.get('https://mean-trello-clone.herokuapp.com/boards/getBoard/'+request);
  
  }
getlist(request) {
    return this._http.get('https://mean-trello-clone.herokuapp.com/lists/getList/'+request);
  
  }
 
 gettasks(request) {
   
    return this._http.get('https://mean-trello-clone.herokuapp.com/tasks/getTasks/'+request)
  
  
  }
 
  crtlist(request) {debugger;
    return this._http.put('https://mean-trello-clone.herokuapp.com/lists/create/'+request[1].id, request[0]);
  
  }
   crttask(request) {debugger;
    return this._http.post('https://mean-trello-clone.herokuapp.com/tasks/create/'+request[1].id, request[0]);
  
  }

}
