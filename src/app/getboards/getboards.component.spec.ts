import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GetboardsComponent } from './getboards.component';

describe('GetboardsComponent', () => {
  let component: GetboardsComponent;
  let fixture: ComponentFixture<GetboardsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GetboardsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GetboardsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
