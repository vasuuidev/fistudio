import { Component, OnInit ,Output, EventEmitter} from '@angular/core';
import { getboards } from './Service';
import { myService } from '../services/ser';
import { NgForm } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-getboards',
  templateUrl: './getboards.component.html',
  styleUrls: ['./getboards.component.css'],
  providers: [getboards]
})
export class GetboardsComponent implements OnInit {
  message: any;

  

 getboards:any;
 closepop:boolean;
 addboardnfg:boolean;
   constructor(private _getboards: getboards,private router: Router,private _myService: myService) { 
     this.addboardnfg=false;
     this.closepop=false
     

   }
   getboarddet()
   {
     var supportbody="";
     this._getboards.getboards(supportbody)
       .subscribe(
       getboards => {
         this.getboards=getboards;
         this.addboardnfg=false;
        
       },
       suppoorteroor => {
              });
   }
   create_board()
   {
     this.addboardnfg=true;
     this.closepop=true;
   }
   
   closeboard()
   {
     this.addboardnfg=false;
   }
   boarddelete(delid)
   {
      this._getboards.deldata(delid) .subscribe(
       res => {
         
             this.getboarddet();
        
       },
       suppoorteroor => {
              });

    
     
   }
   boarddet(p)
   {
      var k =p;
      this._myService.setData(p);

    
     this.router.navigate(['/tasks']);
   }
crb( request, form: NgForm)
{
    var supportbody={
  board_name: form.value.boardname
}
     this._getboards.createboard(supportbody)
       .subscribe(
       getboards => {
         this.getboarddet();
              //this.router.navigate(['/']);
        
       },
       suppoorteroor => {
              });
}
fg()
   {debugger;
     this.addboardnfg=false;
      this.closepop=false;
   }
  
  ngOnInit() {
    this.getboarddet();
    
  }

}
